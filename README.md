# Fieldlab February 2021

We created a cluster for your team so you can work during the Common Ground Fieldlab.

- The cluster passes the Haven Compliancy Checker
- The cluster exists of 3 Standard_DS2_v2 nodes (2 vCPUs / 7GB memory)
- Ingress-nginx, Loki and Cert-manager are pre-installed
- DNS records are created: `*.{cluster-name}.fieldlab.demoground.nl` point to the Ingress Controller of the cluster

You can view the Terraform that is used to create the clusters [here](./terraform). Merge Requests are welcome ;-)

## Connect to the cluster

Make sure you installed [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/). At the start of the fieldlab you will receive one kubeconfig with cluster-admin privileges from one of the Haven team members. You can distribute this kubeconfig to your team.

## The end of the cluster

We will delete the clusters on Monday February 8th at 12:00.


## Prepare for NLX on the cluster


Download the [latest version of NLX](https://gitlab.com/commonground/nlx/nlx/-/releases/v0.94.2).

```sh
wget -P nlx https://gitlab.com/commonground/nlx/nlx/-/archive/v0.94.2/nlx-v0.94.2.zip
unzip -d nlx nlx/nlx-v0.94.2.zip "nlx-v0.94.2/helm/*"
```

Request a demo TLS certificate. The following steps are taken from the [NLX docs](https://docs.nlx.io/try-nlx/retrieve-a-demo-certificate).

```sh
# Replace the <place holders> accordingly
export NAME=<Your name> DOMAIN=inway.<name of cluster>.fieldlab.demoground.nl
openssl req -utf8 -nodes -sha256 -newkey rsa:4096 -keyout nlx/tls/org.key -out nlx/tls/org.csr -subj "/C=NL/O=$NAME/CN=$DOMAIN"
```

Use the [Certportal](https://certportal.demo.nlx.io/) to retrieve a signed certificate. Copy the contents from the `tls/org.csr` file. Also include the the `-----BEGIN CERTIFICATE REQUEST-----` and `----END CERTIFICATE REQUEST-----` lines. Save the downloaded certifcate as `nlx/tls/org.crt`.

```sh
cat nlx/tls/org.csr

# On macOS you can copy it directly to the clipboard
cat nlx/tls/org.csr | pbcopy
```

Store the downloaded TLS certificate as a secret in Kubernetes. This secret can then be refered by chart values as `existingSecret`. Use `nlx-organization-tls` as the secret name.

```sh
# Create NLX organisation TLS secret
kubectl create secret tls nlx-organization-tls --cert nlx/tls/org.crt --key nlx/tls/org.key
```

Some NLX components require interal TLS communication between them. To make it easier to issue those certificates we configure a cluster issuer specific for this. Your cluster has [cert-manager](https://cert-manager.io/) already installed.

```sh
# Create internal CA certificate
openssl req -x509 -sha256 -nodes -newkey rsa:4096 -reqexts v3_req -extensions v3_ca -config nlx/openssl.cnf -keyout nlx/tls/ca.key -out nlx/tls/ca.crt -subj "/C=NL/CN=NLX internal CA"

# Create CA secret
kubectl create secret tls nlx-internal-ca-tls --cert nlx/tls/ca.crt --key nlx/tls/ca.key

# Install internal CA issuer
kubectl apply -f nlx/ca-issuer.yaml
```

To issue certificates for the components you need to create `Certificate` resources. An example resource can be found in `nlx/ca-certificate.example.yaml`. More information can be found in the [cert-manager documentation](https://cert-manager.io/docs/usage/certificate/).

The cluster is now prepared for NLX.

All the NLX components can be installed with Helm. See the `nlx/nlx-v0.94.2/helm/charts` directory for all the available charts. [Install Helm v3+](https://helm.sh/docs/intro/install/) if you don't have it already.

Create a directory to store Helm `values.yaml` files for each chart. These files allow you to overwrite the defaults from the chart and thus customizing your NLX installation. Refer to the documentation of each chart for the available values.

An example how to install an NLX Outway

```sh
# Setup a NLX Outway
helm upgrade --install -f nlx/helm/<your own values-file> nlx-outway nlx/nlx-v0.94.2/helm/charts/nlx-outway
```
