provider "azurerm" {
  features {}
}

terraform {
  backend "azurerm" {
    resource_group_name  = "core-prod"
    storage_account_name = "coreprodterraformstate"
    container_name       = "terraform-state"
    key                  = "fieldlab-feb-2021.tfstate"
  }
}

resource "azurerm_resource_group" "fieldlab-feb-2021" {
  name     = "fieldlab-feb-2021"
  location = "West Europe"
}
